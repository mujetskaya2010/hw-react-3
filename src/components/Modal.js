import React from "react";
import "./Modal.scss";

const Modal = (props) => {
  const {
    header,
    closeButton,
    action,
    onClose,
    btnOne,
    btnSecond,
    onClickOutside,
    children,
  } = props;

  return (
    <div className="modal" onClick={onClickOutside}>
      <div className={"modal-content"}>
        <div className="modal-header">
          <h4>{header}</h4>
          {closeButton && (
            <button className="close-button" onClick={onClose}>
              X
            </button>
          )}
        </div>
        <div className="modal-body">{children}</div>
        <div className="modal-footer">
          <button action={action} onClick={props.onClick}>
            {btnOne}
          </button>
          <button action={action} onClick={onClose}>
            {btnSecond}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
