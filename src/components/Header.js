import React from "react";
import Cart from "./Cart";
import Favorites from "./Favorites";
import "./Header.scss";
import { Link } from "react-router-dom";

const Header = ({
  cartList,
  favoritesList,
  handleRemoveFromCart,
  handleRemoveFromFavorites,
}) => {
  return (
    <div>
      <nav className="navbar ">
        <div className="container-fluid">
          <Link to="/" className="navbar-brand">
            <img
              src="https://www.seekpng.com/png/full/308-3085942_barbie-logo-png-download-barbie-logo-png.png"
              alt="Barbie-logo"
              width={120}
            />
          </Link>

          <div className="header-icons">
            {" "}
            <Cart
              cartList={cartList}
              handleRemoveFromCart={handleRemoveFromCart}
            />
            <Favorites
              favoritesList={favoritesList}
              handleRemoveFromFavorites={handleRemoveFromFavorites}
            />
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Header;
