import React, { createContext, useEffect, useState } from "react";
import axios from "axios";

export const DataListContext = createContext();

const DataListProvider = ({ children }) => {
  const [dataList, setDataList] = useState([]);
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      const response = await axios.get("products.json");
      setDataList(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  return (
    <DataListContext.Provider value={{ dataList }}>
      {children}
    </DataListContext.Provider>
  );
};

export default DataListProvider;
