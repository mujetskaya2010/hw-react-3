import React from "react";
import Header from "./components/Header";
import CardList from "./components/CardList";
import DataListProvider from "./components/DataListContext";
import PropTypes from "prop-types";

const App = ({
  cartList,
  favoritesList,
  handleRemoveFromCart,
  handleRemoveFromFavorites,
  addToFavorites,
  addToCart,
  saveDataToLocalStorage,
}) => {
  return (
    <>
      <Header cartList={cartList} favoritesList={favoritesList} />
      <DataListProvider>
        <CardList
          handleRemoveFromCart={handleRemoveFromCart}
          handleRemoveFromFavorites={handleRemoveFromFavorites}
          addToFavorites={addToFavorites}
          addToCart={addToCart}
          saveDataToLocalStorage={saveDataToLocalStorage}
        />
      </DataListProvider>
    </>
  );
};

App.propTypes = {
  cartList: PropTypes.array.isRequired,
  favoritesList: PropTypes.array.isRequired,
  handleRemoveFromCart: PropTypes.func.isRequired,
  handleRemoveFromFavorites: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
};

export default App;
